cmake_minimum_required(VERSION 3.17)

project(cmake_modern_template VERSION 0.0.1 LANGUAGES CXX)

add_subdirectory(source)
