#include <iostream>

#include <lib1.hpp>
#include <lib2.hpp>

#include <static.hpp>

int main()
{
	std::cout << 	"Hello! The following functions are being called:\n"
			    << function_one() <<
			" " << function_two() <<
			" " << function_three() <<
			" " << function_four() <<
			" " << function_five() <<
			" " << function_six()
			<< std::endl;
	return 0;
}
